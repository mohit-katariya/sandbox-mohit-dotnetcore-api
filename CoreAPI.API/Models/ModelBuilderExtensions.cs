﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.API.Models
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(
                new Category { Id = 1, Name = "Category 1" },
                new Category { Id = 2, Name = "Category 2" },
                new Category { Id = 3, Name = "Category 3" },
                new Category { Id = 4, Name = "Category 4" },
                new Category { Id = 5, Name = "Category 5" }
            );
            modelBuilder.Entity<Product>().HasData(
                new Product { Id = 1, CategoryId = 1, Name = "Product 1", Description = "Description 1", Price = 10, Sku = "Sku 1", IsAvailable = true },
                new Product { Id = 2, CategoryId = 1, Name = "Product 2", Description = "Description 2", Price = 10, Sku = "Sku 2", IsAvailable = true },
                new Product { Id = 3, CategoryId = 1, Name = "Product 3", Description = "Description 3", Price = 11, Sku = "Sku 3", IsAvailable = true },
                new Product { Id = 4, CategoryId = 1, Name = "Product 4", Description = "Description 4", Price = 12, Sku = "Sku 4", IsAvailable = true },
                new Product { Id = 5, CategoryId = 1, Name = "Product 5", Description = "Description 5", Price = 13, Sku = "Sku 5", IsAvailable = true },
                new Product { Id = 6, CategoryId = 2, Name = "Product 6", Description = "Description 6", Price = 14, Sku = "Sku 6", IsAvailable = true },
                new Product { Id = 7, CategoryId = 2, Name = "Product 7", Description = "Description 7", Price = 15, Sku = "Sku 7", IsAvailable = true },
                new Product { Id = 8, CategoryId = 2, Name = "Product 8", Description = "Description 8", Price = 16, Sku = "Sku 8", IsAvailable = true },
                new Product { Id = 9, CategoryId = 2, Name = "Product 9", Description = "Description 9", Price = 17, Sku = "Sku 9", IsAvailable = true },
                new Product { Id = 10, CategoryId = 2, Name = "Product 10", Description = "Description 10", Price = 18, Sku = "Sku 10", IsAvailable = true },
                new Product { Id = 11, CategoryId = 3, Name = "Product 11", Description = "Description 11", Price = 19, Sku = "Sku 11", IsAvailable = true },
                new Product { Id = 12, CategoryId = 3, Name = "Product 12", Description = "Description 12", Price = 11, Sku = "Sku 12", IsAvailable = true },
                new Product { Id = 13, CategoryId = 3, Name = "Product 13", Description = "Description 13", Price = 12, Sku = "Sku 13", IsAvailable = true },
                new Product { Id = 14, CategoryId = 3, Name = "Product 14", Description = "Description 14", Price = 13, Sku = "Sku 14", IsAvailable = true },
                new Product { Id = 15, CategoryId = 3, Name = "Product 15", Description = "Description 15", Price = 14, Sku = "Sku 15", IsAvailable = true },
                new Product { Id = 16, CategoryId = 4, Name = "Product 16", Description = "Description 16", Price = 15, Sku = "Sku 16", IsAvailable = true },
                new Product { Id = 17, CategoryId = 4, Name = "Product 17", Description = "Description 17", Price = 16, Sku = "Sku 17", IsAvailable = true },
                new Product { Id = 18, CategoryId = 4, Name = "Product 18", Description = "Description 18", Price = 17, Sku = "Sku 18", IsAvailable = true },
                new Product { Id = 19, CategoryId = 4, Name = "Product 19", Description = "Description 19", Price = 18, Sku = "Sku 19", IsAvailable = true },
                new Product { Id = 20, CategoryId = 4, Name = "Product 20", Description = "Description 20", Price = 19, Sku = "Sku 20", IsAvailable = true },
                new Product { Id = 21, CategoryId = 5, Name = "Product 21", Description = "Description 21", Price = 20, Sku = "Sku 21", IsAvailable = true },
                new Product { Id = 22, CategoryId = 5, Name = "Product 22", Description = "Description 22", Price = 10, Sku = "Sku 22", IsAvailable = true },
                new Product { Id = 23, CategoryId = 5, Name = "Product 23", Description = "Description 23", Price = 11, Sku = "Sku 23", IsAvailable = true },
                new Product { Id = 24, CategoryId = 5, Name = "Product 24", Description = "Description 24", Price = 12, Sku = "Sku 24", IsAvailable = true },
                new Product { Id = 25, CategoryId = 5, Name = "Product 25", Description = "Description 25", Price = 13, Sku = "Sku 25", IsAvailable = true }
            );
            modelBuilder.Entity<User>().HasData(
                new User { Id = 1, Email = "test1@test.com" },
                new User { Id = 2, Email = "test2@test.com" }
                );
        }
    }
}
