﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreAPI.API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CoreAPI.API.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ShopContext context;

        public ProductsController(ShopContext context)
        {
            this.context = context;
            this.context.Database.EnsureCreated();
        }
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetAllProducts()//([FromQuery] ProductQueryParameters queryParameters)
        {
            IQueryable<Product> products = context.Products;
            //if (queryParameters.MinPrice != null && queryParameters.MaxPrice != null)
            //{
            //    products = products.Where(p => p.Price >= queryParameters.MinPrice.Value && p.Price <= queryParameters.MaxPrice.Value);
            //}
            //if (!string.IsNullOrEmpty(queryParameters.Sku))
            //{
            //    products = products.Where(p => p.Sku == queryParameters.Sku);
            //}


            //products = products
            //    .Skip(queryParameters.Size * (queryParameters.Page - 1))
            //    .Take(queryParameters.Size);
            return Ok(await products.ToArrayAsync());
        }
        [HttpGet("{Id}")]
        public async Task<IActionResult> GetProductById(int Id)
        {
            var product = context.Products.FindAsync(Id);
            if (product == null)
                return NotFound();
            else
                return Ok(await product);

        }
    }
}